<?php

include 'includes/header.php';

require_once '../vendor/autoload.php';

use App\Model\ProductRepository;
use App\Model\ReviewRepository;

    $id = $_REQUEST['id'];
    $productRepo = new ProductRepository();
    $product = $productRepo->getProductById($id);

    if ($product === false) {
        echo "<div class='alert alert-warning' role='alert'> No result, try with another id</div>";
        exit();
    }


    $reviewRepo = new ReviewRepository();
    $reviews = $reviewRepo->getReviewsByProductId($id);

    $allReviewsRepo = new ReviewRepository();
    $allReviews = $allReviewsRepo->countSameReviews($id);
//    dd($allReviews);

?>

<main class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <img src="img/<?php echo $product->getImageName() ?>" alt="php" style="max-width: 300px; margin-bottom: 20px"
                    <span><h3 class="card-title"><?php echo $product->getName(); ?><span class="badge bg-danger rounded-pill"><?php echo $product->getPrice()?> $</span></h3></span>
                    <p class="card-text"><?php echo $product->getDescription() ?></p>
                    <a href="#" class="btn btn-primary">Buy</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <h3>Product Rating</h3>
            <ul class="list-group">
                <?php foreach ($allReviews as $rate) { ?>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span class="badge bg-dark rounded-pill">⭐⭐⭐⭐⭐</span>
                    <?= $rate ?>
                </li>
                <?php } ?>
            </ul>

        </div>
    </div>

    <br>
    <hr>


    <h3>Customer Reviews</h3>
    <ul class="list-group">
        <?php foreach ($reviews as $review) { ?>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <p><?php echo $review->getBody() ?> <i class="text-muted"> <?= $review->getPublicationDate() ?> </i></p>
            <span class="badge bg-dark rounded-pill"><?= str_repeat('⭐', $review->getRating()) ?> </span>
        </li>
        <?php } ?>
    </ul>
</main>











