<?php

namespace App\Model;

use App\Model\ModelRepository;

class ProductRepository extends ModelRepository
{
    public function getProductById(int $id): false|Product
    {
        $stmt = $this->getPdo()->prepare("SELECT * FROM product WHERE id = $id");
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_CLASS, Product::class);
        $product = $stmt->fetch();



        if ($product === false) {
            return false;
        }

        return $product;

    }


}