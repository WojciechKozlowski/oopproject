<?php

namespace App\Model;

class ReviewRepository extends ModelRepository
{
    public function getReviewsByProductId(int $id): array
    {
        {
            $stmt = $this->getPdo()->prepare("SELECT * FROM review WHERE product_id = $id");
            $stmt->execute();
            $reviews = $stmt->fetchAll(\PDO::FETCH_CLASS, Review::class);
            return $reviews;
        }
    }

    public function countSameReviews($id): array
    {
        $stmt = $this->getPdo()->prepare("SELECT rating FROM review WHERE product_id = $id");
        $stmt->execute();
        $allReviews = $stmt->fetchAll();
        $columnReviews = array_column($allReviews, 'rating');
        $allRatesCounted = array_count_values($columnReviews);
        krsort($allRatesCounted);
        return $allRatesCounted;
    }

}